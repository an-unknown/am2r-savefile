package org.am2r;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Crypt {
	private static char getch(String s, int i) {
		return i == 0 ? s.charAt(0) : s.charAt(i - 1);
	}

	public static byte[] crypt(byte[] file, String key, int rate) {
		String gmid = "971912648"; // game_id
		byte[] keys = new byte[gmid.length() * 5];
		int keypos = 0;
		int epos = 0;
		rate = 10 < rate ? 10 : rate; // min(rate, 10)
		rate = 0 > rate ? 0 : rate; // max(rate, 0)
		rate = (int) Math.round(rate * file.length / 10000.0);
		for(int i = 1; i < 5; i++)
			gmid += gmid;
		for(int i = 0; i < keys.length; i++) {
			keys[epos] = (byte) (getch(gmid, epos) ^ (getch(key, keypos++) - (epos / 3)));
			if(keypos > key.length())
				keypos = 1;
			epos++;
		}
		epos--;
		keypos = 0;
		for(int pos = 0; pos < file.length; pos++) {
			byte read = file[pos];
			file[pos] = (byte) (read ^ keys[keypos++]);
			if(keypos > epos)
				keypos = 1;
			if(rate != 0)
				pos += rate;
		}
		return file;
	}

	public static void main(String[] args) throws IOException {
		InputStream in = new FileInputStream("sav1");
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buf = new byte[256];
		int nbytes;
		while((nbytes = in.read(buf)) != -1)
			bos.write(buf, 0, nbytes);
		in.close();
		bos.close();
		buf = bos.toByteArray();

		byte[] data = crypt(buf, "XOR_DFJykQ8xX3PuNnkLt6QviqALOLF8cxIDx1D63DAdph4KGQ4rOJ7", 2);
		OutputStream out = new FileOutputStream("sav1.dec");
		out.write(data);
		out.close();
	}
}
