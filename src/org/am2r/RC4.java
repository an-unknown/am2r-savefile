package org.am2r;

public class RC4 {
	private final byte[] S = new byte[256];
	private byte i, j;

	public RC4(byte[] key) {
		seed(key);
	}

	protected void seed(byte[] key) {
		for(int i = 0; i < S.length; i++) {
			S[i] = (byte) i;
		}
		int j = 0;
		for(int i = 0; i < S.length; i++) {
			j = Byte.toUnsignedInt((byte) (j + S[i] + key[i % key.length]));
			byte tmp = S[i];
			S[i] = S[j];
			S[j] = tmp;
		}
		this.i = 0;
		this.j = 0;
	}

	public byte getKeyByte() {
		i++;
		j += S[Byte.toUnsignedInt(i)];
		byte tmp = S[Byte.toUnsignedInt(j)];
		S[Byte.toUnsignedInt(j)] = S[Byte.toUnsignedInt(i)];
		S[Byte.toUnsignedInt(i)] = tmp;
		return S[Byte.toUnsignedInt((byte) (S[Byte.toUnsignedInt(i)] + S[Byte.toUnsignedInt(j)]))];
	}

	public byte[] encrypt(byte[] input) {
		byte[] output = new byte[input.length];
		for(int i = 0; i < input.length; i++) {
			output[i] = (byte) (input[i] ^ getKeyByte());
		}
		return output;
	}
}
