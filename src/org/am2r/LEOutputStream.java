package org.am2r;

import java.io.IOException;
import java.io.OutputStream;

public class LEOutputStream extends OutputStream {
	private OutputStream parent;

	public LEOutputStream(OutputStream parent) {
		this.parent = parent;
	}

	@Override
	public void close() throws IOException {
		parent.close();
	}

	@Override
	public void flush() throws IOException {
		parent.flush();
	}

	@Override
	public void write(int value) throws IOException {
		parent.write(value);
	}

	@Override
	public void write(byte[] buffer) throws IOException {
		parent.write(buffer);
	}

	@Override
	public void write(byte[] buffer, int offset, int length)
			throws IOException {
		parent.write(buffer, offset, length);
	}

	public void write8bit(byte value) throws IOException {
		write(value);
	}

	public void write16bit(short value) throws IOException {
		parent.write(Endianess.set16bit_LE(value, new byte[2]));
	}

	public void write32bit(int value) throws IOException {
		parent.write(Endianess.set32bit_LE(value, new byte[4]));
	}

	public void write32bit(float value) throws IOException {
		parent.write(Endianess.set32bit_LE(value, new byte[4]));
	}

	public void write64bit(long value) throws IOException {
		parent.write(Endianess.set64bit_LE(value, new byte[8]));
	}

	public void write64bit(double value) throws IOException {
		parent.write(Endianess.set64bit_LE(value, new byte[8]));
	}
}
