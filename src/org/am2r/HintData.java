package org.am2r;

import java.io.IOException;

public class HintData {
	private boolean[] hints;

	public HintData() {
		hints = new boolean[20];
	}

	public HintData(DsList list) {
		read(list);
	}

	public HintData(String data) throws IOException {
		this(new DsList(data));
	}

	public void read(DsList list) {
		hints = new boolean[20];
		for(int i = 0; i < hints.length; i++)
			hints[i] = list.getDouble(i) != 0.0;
	}

	public DsList write() {
		DsList list = new DsList(hints.length);
		for(int i = 0; i < hints.length; i++)
			list.setDouble(i, hints[i] ? 1.0 : 0.0);
		return list;
	}

	public void set(HintData other) {
		for(int i = 0; i < hints.length; i++)
			setHint(i, other.getHint(i));
	}

	public boolean getHint(int i) {
		return hints[i];
	}

	public void setHint(int i, boolean hint) {
		hints[i] = hint;
	}

	public int size() {
		return hints.length;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder("HintData[");
		for(int i = 0; i <  size(); i++) {
			if(i > 0)
				b.append(',');
			b.append(getHint(i) ? '1' : '0');
		}
		return b.append("]").toString();
	}
}
