package org.am2r.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.am2r.EventData;

public class EventEditor extends JPanel {
	private static final long serialVersionUID = 1L;

	private static final int[] ids = Names.getEventIDs();

	private Model model;
	private JTable table;

	private class Model extends AbstractTableModel {
		private static final long serialVersionUID = 1L;

		private EventData data;

		public Model(EventData data) {
			this.data = data;
		}

		public void read(EventData data) {
			this.data = data;
			fireTableDataChanged();
		}

		@Override
		public int getColumnCount() {
			return 2;
		}

		@Override
		public int getRowCount() {
			return ids.length; // 350
		}

		@Override
		public Object getValueAt(int row, int col) {
			int id = ids[row];
			if(col == 0)
				return Names.getEvent(id);
			return data.getEvent(id);
		}

		@Override
		public void setValueAt(Object value, int row, int col) {
			if(col == 0)
				return;
			int id = ids[row];
			String val = String.valueOf(value);
			try {
				data.setEvent(id, Double.parseDouble(val));
			} catch(NumberFormatException e) {
			}
		}

		@Override
		public String getColumnName(int col) {
			return col == 0 ? "Name" : "Value";
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			return col > 0;
		}
	}

	public EventEditor(EventData data) {
		super(new BorderLayout());
		model = new Model(data);
		table = new MixedTable(model);
		add(BorderLayout.CENTER, new JScrollPane(table));
	}

	public void read(EventData data) {
		model.read(data);
	}
}
