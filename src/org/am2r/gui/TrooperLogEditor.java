package org.am2r.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.am2r.TrooperLogData;

public class TrooperLogEditor extends JPanel {
	private static final long serialVersionUID = 1L;

	private Model model;
	private JTable table;

	private class Model extends ExtendedTableModel {
		private static final long serialVersionUID = 1L;

		private final String[] CAPTIONS = { "Name", "Value" };

		private TrooperLogData data;

		public Model(TrooperLogData data) {
			this.data = data;
		}

		public void read(TrooperLogData data) {
			this.data = data;
			fireTableDataChanged();
		}

		@Override
		public int getColumnCount() {
			return 2;
		}

		@Override
		public int getRowCount() {
			return data.size();
		}

		@Override
		public Object getDisplayValueAt(int row, int col) {
			if(col != 0) {
				int id = data.getLog(7 - row);
				return Names.getTrooperLog(id) + " [" + id + "]";
			}
			return getValueAt(row, col);
		}

		@Override
		public Object getValueAt(int row, int col) {
			if(col == 0)
				return "Log " + (row + 1);
			else
				return data.getLog(7 - row);
		}

		@Override
		public void setValueAt(Object value, int row, int col) {
			if(col == 0)
				return;
			String val = String.valueOf(value);
			try {
				data.setLog(7 - row, Integer.parseInt(val));
			} catch(NumberFormatException e) {
			}
		}

		@Override
		public String getColumnName(int col) {
			return CAPTIONS[col];
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			return col > 0;
		}
	}

	public TrooperLogEditor(TrooperLogData data) {
		super(new BorderLayout());
		model = new Model(data);
		table = new MixedTable(model);
		add(BorderLayout.CENTER, new JScrollPane(table));
	}

	public void read(TrooperLogData data) {
		model.read(data);
	}
}
