package org.am2r.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.am2r.Save;

public class MergeWindow {
	private JDialog dialog;

	private Model model;
	private JTable table;

	private static final int MAIN = 0;
	private static final int INVENTORY = 1;
	private static final int ITEMS = 2;
	private static final int EVENTS = 3;
	private static final int LOGS = 4;
	private static final int HINTS = 5;
	private static final int METROIDS = 6;
	private static final int MAP = 7;
	private static final String[] NAMES = { "Main", "Inventory", "Items", "Events", "Logs", "Hints", "Metroids",
			"Map" };
	private boolean[] flags;

	private Save ours;
	private Save others;

	private class Model extends AbstractTableModel {
		private static final long serialVersionUID = 1L;

		Model() {
			flags = new boolean[NAMES.length];
			Arrays.fill(flags, false);
		}

		@Override
		public int getColumnCount() {
			return 2;
		}

		@Override
		public int getRowCount() {
			return NAMES.length;
		}

		@Override
		public Object getValueAt(int row, int col) {
			return col == 0 ? NAMES[row] : flags[row];
		}

		@Override
		public void setValueAt(Object value, int row, int col) {
			if(col == 0)
				return;
			String val = String.valueOf(value);
			flags[row] = Boolean.parseBoolean(val);
		}

		@Override
		public String getColumnName(int col) {
			return col == 0 ? "Section" : "Copy?";
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			return col > 0;
		}
	}

	public MergeWindow(JFrame parent, Save ours, Save others) {
		this.ours = ours;
		this.others = others;
		dialog = new JDialog(parent, "Merge save files", true);
		dialog.setLayout(new BorderLayout());
		model = new Model();
		table = new MixedTable(model);
		dialog.add(BorderLayout.CENTER, new JScrollPane(table));
		JPanel buttons = new JPanel(new FlowLayout());
		JButton ok = new JButton("OK");
		JButton cancel = new JButton("Cancel");
		ok.addActionListener((e) -> accept());
		cancel.addActionListener((e) -> close());
		buttons.add(ok);
		buttons.add(cancel);
		dialog.add(BorderLayout.SOUTH, buttons);
		dialog.setSize(300, 300);
	}

	public void show() {
		dialog.setVisible(true);
	}

	public void close() {
		dialog.setVisible(false);
		dialog.dispose();
	}

	private void accept() {
		merge();
		close();
	}

	private void merge() {
		if(flags[MAIN]) {
			ours.getMain().set(others.getMain());
		}
		if(flags[INVENTORY]) {
			ours.getInventory().set(others.getInventory());
		}
		if(flags[ITEMS]) {
			ours.getItems().set(others.getItems());
		}
		if(flags[EVENTS]) {
			ours.getEvents().set(others.getEvents());
		}
		if(flags[LOGS]) {
			ours.getLogs().set(others.getLogs());
		}
		if(flags[HINTS]) {
			ours.getHints().set(others.getHints());
		}
		if(flags[METROIDS]) {
			ours.getMetroids().set(others.getMetroids());
		}
		if(flags[MAP]) {
			ours.getMap().set(others.getMap());
		}
	}
}
