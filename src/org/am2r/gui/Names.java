package org.am2r.gui;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Names {
	private static final Map<Integer, String> events;
	private static final Map<Integer, String> items;
	private static final Map<Integer, String> logs;
	private static final Map<Integer, String> trooperlogs;
	private static final Map<Integer, String> hints;
	private static final Map<Integer, String> metroids;
	private static final Map<Integer, String> randomizer;
	public static final String[] SAVE_STATION_ROOMS = { "rm_a0h01", "rm_a0h03a", "rm_a0h04c", "rm_a1h02",
			"rm_a1a01", "rm_a1h07", "rm_a2a01A", "rm_a2c02", "rm_a2a03", "rm_a3h01", "rm_a3a02", "rm_a3a17",
			"rm_a0h19", "rm_a0h26", "rm_a4a02", "rm_a4h14", "rm_a4b12", "rm_a5c03", "rm_a5c19", "rm_a5c28",
			"rm_a0h33", "rm_a6a15", "rm_a6b06", "rm_a6b16", "rm_a7b01", "rm_a8a14" };
	public static final String[] SAVE_STATIONS = { "0 - Landing Site", "1 - Surface Cave #1", "2 - Surface Cave #2",
			"3 - Golden Temple Breeding Grounds", "4 - The Golden Temple #1", "5 - The Golden Temple #2",
			"6 - Hydro Station #1", "7 - Hydro Station Breeding Grounds", "8 - Hydro Station #2",
			"9 - Eastern Cave", "10 - Robot Factory Entrance", "11 - Robot Factory Storage",
			"12 - Mining Facility", "13 - Western Cave", "14 - Tower Entrance", "15 - Tower Exterior",
			"16 - Geothermal Plant", "17 - Energy Distribution Entrance", "18 - Main Distribution Facility",
			"19 - Distribution Facility Storage", "20 - Main Cave Depths", "21 - Omega Nest Entrance",
			"22 - The Omega Nest", "23 - Omega Nest Exit", "24 - Genetics Laboratory", "25 - GFS Thoth" };

	static {
		events = new HashMap<Integer, String>();
		events.put(0, "A0: Metroid1 appeared");
		events.put(2, "A0: Lava on 4c lowered");
		events.put(3, "A0: First red door");
		events.put(4, "A0: Metroid scan 1");
		events.put(50, "A1: boss active");
		events.put(51, "A1: boss defeated");
		events.put(52, "A1: red door - bombs");
		events.put(53, "A1: red door - charge beam");
		events.put(54, "A1: grapple puzzle solved");
		events.put(56, "A1: Metroid scan 2");
		events.put(57, "A1: a06 door switch");
		events.put(58, "A1: first etank orb switch");
		events.put(59, "A1: Yellow door above boss");
		events.put(101, "A2: Turbine destroyed (water level: 0-4)");
		events.put(102, "A2: Water propulsor");
		events.put(103, "A2: Arachnus Defeated");
		events.put(104, "A2: red door - Arachnus");
		events.put(105, "A2: red door - varia");
		events.put(106, "A2: red door - wave beam");
		events.put(107, "A2: red door - hi jump");
		events.put(108, "A2: surprise metroid");
		events.put(109, "A2: gamma appeared");
		events.put(110, "A2: Locked room switch");
		events.put(151, "A3: Red Door - Space Jump");
		events.put(152, "A3: Torizo defeated");
		events.put(153, "A3: Red door - Spazer");
		events.put(154, "A3: Red door - SpdBooster");
		events.put(155, "A3: Metroid scan 3");
		events.put(156, "A3: SMissile Room Cleared");
		events.put(159, "A3: Crystal - SM Room");
		events.put(160, "A3: Autoad first use (hint)");
		events.put(161, "A3: Surprise Gamma");
		events.put(162, "A3: Green door, big cave");
		events.put(163, "A3: Lock first save");
		events.put(170, "A3A/B: Lab storage room visited");
		events.put(171, "A3A/B: Dual alphas discovered");
		events.put(172, "A3A/B: Drill Used (0-3)");
		events.put(173, "A3A/B: Metroid scan 4");
		events.put(174, "A3A/B: Crystal - Mines Entrance");
		events.put(175, "A3A/B: Crystal - Drill");
		events.put(176, "A3A/B: Lab passage clear (open elevator)");
		events.put(200, "A4: Power enabled (0 off, 1 on, 2 tester defeated)");
		events.put(201, "A4: red door, pbeam");
		events.put(202, "A4: yellow door west");
		events.put(203, "A4: escape sequence (9 is complete)");
		events.put(204, "A4: Metroid scan 5");
		events.put(205, "A4: Zeta intro");
		events.put(206, "A4: Break rocks on way to area 4");
		events.put(250, "A5: Distribution enabled");
		events.put(251, "A5: c09 switch (first)");
		events.put(252, "A5: c10 switch (enemies)");
		events.put(253, "A5: c11 switch (shaft, shortcut)");
		events.put(254, "A5: c15 switch (upper entrance)");
		events.put(255, "A5: c17 save2 red door (to screw attack area)");
		events.put(256, "A5: c21 switch (first on way to SA)");
		events.put(257, "A5: c23 s.attack red door");
		events.put(258, "A5: c32 ice beam red door");
		events.put(259, "A5: Metroid scan 6");
		events.put(260, "A5: b02 red door (to ice beam area)");
		events.put(261, "A5: boss state (defeated Serris)");
		events.put(262, "A5: c33 switch");
		events.put(263, "A5: a05 upper red door (after gravity suit)");
		events.put(264, "A5: a05 lower red door (to gravity suit)");
		events.put(300, "A6/7/8: Omega metroid appears");
		events.put(301, "A6/7/8: metroid scan");
		events.put(302, "A6/7/8: Egg discovered");
		events.put(303, "A6/7/8: Queen defeated");
		events.put(304, "A6/7/8: Egg hatched");
		events.put(305, "A6/7/8: Path to first Omega");
		events.put(306, "A6/7/8: Metroid scan)");
		events.put(307, "A6/7/8: Genesis");

		items = new HashMap<Integer, String>();
		items.put(0, "Bombs");
		items.put(1, "Morph Ball");
		items.put(2, "SpBall");
		items.put(3, "JmpBall");
		items.put(4, "HiJump");
		items.put(5, "Varia");
		items.put(6, "SpJump");
		items.put(7, "SBoost");
		items.put(8, "Scrwat");
		items.put(9, "GravSt");
		items.put(10, "Charge Beam");
		items.put(11, "Ice Beam");
		items.put(12, "Wave Beam");
		items.put(13, "Spazer Beam");
		items.put(14, "Plasma Beam");
		items.put(50, "Misc: Energy Tank by Genesis");
		items.put(51, "Misc: Super missile after first Omega Metroid");
		items.put(52, "Misc: Bottom missile in 1x2 room on way to area 2");
		items.put(53, "Misc: Top missile in same room");
		items.put(54, "Misc: Missile on way to Area 4 (speed boost up)");
		items.put(55, "Misc: Other missile on way to A4 (speed to the right)");
		items.put(56, "Misc: First missile on way to final area");
		items.put(57, "Misc: Missile in 4x4 room on way to area 2");
		items.put(58, "Misc: PB on left side of big surface room");
		items.put(59, "Misc: PB on right side of big surface room");
		items.put(60, "Misc: Last missile on way to final area");
		items.put(100, "A1: Missile by bombs");
		items.put(101, "A1: Left missile in top 3 item room");
		items.put(102, "A1: Right missile in top 3 item room");
		items.put(103, "A1: Energy Tank just after getting bombs");
		items.put(104, "A1: Missile on left side of right 3 item room");
		items.put(105, "A1: Bottom missile in right 3 item room");
		items.put(106, "A1: Top missile in right 3 item room");
		items.put(107, "A1: Missile with 4 bomb blocks (room to the right of 104-106)");
		items.put(108, "A1: Energy Tank (same room as 101-102)");
		items.put(109, "A1: Missiles in top of large room");
		items.put(110, "A1: Super missile in drone door");
		items.put(111, "A1: Missile above guardian");
		items.put(112, "A1: Power bomb above guardian");
		items.put(150, "A2: Missile above water dams");
		items.put(151, "A2: Missile above water/metroid");
		items.put(152, "A2: Missile by Arachnus");
		items.put(153, "A2: Top missile in 2 item room");
		items.put(154, "A2: Bottom missile in 2 item room");
		items.put(155, "A2: Missile in left side of bottom room");
		items.put(156, "A2: Missile by tank/hi-jump boots");
		items.put(157, "A2: Energy Tank in same room as 156");
		items.put(158, "A2: Energy Tank by metroids");
		items.put(159, "A2: Missile by Varia suit");
		items.put(160, "A2: Power bomb (shinespark above metroid)");
		items.put(161, "A2: Missile in same room as 160");
		items.put(162, "A2: Super missile in drone room");
		items.put(163, "A2: Missile in big open room (on right side)");
		items.put(200, "A3: Energy Tank in wall by the large room");
		items.put(201, "A3: Energy Tank by Aplha (conveyer belt)");
		items.put(202, "A3: Missiles in wall item 200");
		items.put(203, "A3: Missiles in sand by save point");
		items.put(204, "A3: Missile above Gamme (speed boost)");
		items.put(205, "A3: Shine through ceiling by rock");
		items.put(206, "A3: First super missile");
		items.put(207, "A3: Super missile under floor of room by save");
		items.put(208, "A3: Missile in same room as 207");
		items.put(209, "A3: Super missile in dark area");
		items.put(210, "A3: Missiles in morph room on way to area 3");
		items.put(211, "A3: Missiles in sand in LL corner of big room");
		items.put(212, "A3: Power bombs behind green door");
		items.put(213, "A3: Power Bombs in drone room");
		items.put(214, "A3: Missiles in drone room");
		items.put(215, "A3: Super missile in research lab");
		items.put(250, "A4: Missile under lava on right side of tower");
		items.put(251, "A4: Energy Tank under giant statues");
		items.put(252, "A4: Missile by 251");
		items.put(253, "A4: First Power Bomb");
		items.put(254, "A4: Energy Tank destroyed power bomb area");
		items.put(255, "A4: Missile in same room as 254");
		items.put(256, "A4: Super missiles (PB after tester)");
		items.put(257, "A4: Missile at very top of area");
		items.put(258, "A4: Power bomb in LR corner of big room");
		items.put(259, "A4: Missile on way to tube to A2 (screw attack through wall)");
		items.put(300, "A5: Super missile in room just after drone activator");
		items.put(301, "A5: PB on way to ice beam (speed to the left)");
		items.put(302, "A5: PB on top of big room on the right");
		items.put(303, "A5: Missile that's 3 rooms to the right of Serris (spider ball)");
		items.put(304, "A5: Missile by 305 & 306");
		items.put(305, "A5: Super missile by 306");
		items.put(306, "A5: Energy tank ");
		items.put(307, "A5: Missile in room with all of the speed blocks");
		items.put(308, "A5: Missile on way to gravity suit");
		items.put(309, "A5: Missile at very top of big room on left");

		logs = new HashMap<Integer, String>();
		logs.put(0, "Mission Briefing");
		logs.put(1, "Planet SR388");
		logs.put(2, "Magma");
		logs.put(3, "Earthquakes");
		logs.put(4, "Research Team");
		logs.put(5, "Rescue Team");
		logs.put(10, "Metroid Species");
		logs.put(11, "Mutations");
		logs.put(12, "Alpha Metroid");
		logs.put(13, "Gamma Metroid");
		logs.put(14, "Zeta Metroid");
		logs.put(15, "Omega Metroid");
		logs.put(16, "Queen Metroid");
		logs.put(20, "The Chozo");
		logs.put(21, "The Golden Temple");
		logs.put(22, "Breeding Grounds");
		logs.put(23, "Hydro Station");
		logs.put(24, "Industrial Complex");
		logs.put(25, "Mining Facility");
		logs.put(26, "The Tower");
		logs.put(27, "Power Plant");
		logs.put(28, "Flooded Temple");
		logs.put(29, "Queen\'s Lair");
		logs.put(30, "Native Species");
		logs.put(31, "Chozo Robots");
		logs.put(32, "Ancient Guardian");
		logs.put(33, "Arachnus");
		logs.put(34, "Torizo");
		logs.put(35, "The Tester");
		logs.put(36, "Tank Prototype");
		logs.put(37, "Armored Boss");
		logs.put(38, "Water Boss");

		logs.put(41, "Researcher's notes: Nuclei");
		logs.put(42, "Researcher's notes: Adaptions");
		logs.put(43, "Researcher's notes: Radioactivity");
		logs.put(44, "Chozo message");
		logs.put(45, "Troopers \"main\" log");

		trooperlogs = new HashMap<Integer, String>();
		trooperlogs.put(0, "not collected");
		trooperlogs.put(1, "GSGT S. Oracca");
		trooperlogs.put(2, "CAPT A. Dallas");
		trooperlogs.put(3, "PFC G. Dutch");
		trooperlogs.put(4, "FR V. Kainé");
		trooperlogs.put(5, "GFR J. Knolan");
		trooperlogs.put(6, "1LT N. Palmer");
		trooperlogs.put(7, "PFC M. Hudson");
		trooperlogs.put(8, "SPC F. Mac");

		hints = new HashMap<Integer, String>();
		hints.put(0, "Diagonal Aiming");
		hints.put(1, "Morph Ball");
		hints.put(2, "Aiming Downwards");
		hints.put(3, "Shooting Missiles");
		hints.put(4, "Autoad");

		metroids = new HashMap<Integer, String>();
		metroids.put(0, "A0-A-Initial Alpha");
		metroids.put(1, "A1-A-Above spiderball");
		metroids.put(2, "A1-A-Breeding grounds upper right");
		metroids.put(3, "A1-A-Breeding grounds left");
		metroids.put(4, "A1-A-Breeding grounds lower right");
		metroids.put(5, "A2-A-Suprise Alpha in rocks");
		metroids.put(6, "A2-A-In shallow water below Varia suit");
		metroids.put(7, "A2-A-Under Arachnus");
		metroids.put(8, "A2-A-Upper left (from large room)");
		metroids.put(9, "A2-A-Breeding grounds upper right");
		metroids.put(10, "A2-A-Breeding grounds left");
		metroids.put(11, "A2-A-Breeding grounds lower right");
		metroids.put(12, "A2-G-First Gamma");
		metroids.put(13, "A3-G-Just before speed booster");
		metroids.put(14, "A3-A-Before energy tank/conveyor");
		metroids.put(15, "A3-G-Lava near first super missile");
		metroids.put(16, "A3-G-Just below SM blocks at start of area");
		metroids.put(17, "A3-G-In room where you need to bomb on the left to proceed");
		metroids.put(18, "A3-A-Below and to the left of Spazer Beam");
		metroids.put(19, "A3-G-Below and to the right of the Spazer Beam");
		metroids.put(20, "A3-G-Dark area below bomb blocks");
		metroids.put(21, "A3-G-End of Dark area");
		metroids.put(22, "A3-G-Right most Gamma from big room");
		metroids.put(23, "Lab-A-Dual Alpha A");
		metroids.put(24, "Lab-A-Dual Alpha B");
		metroids.put(25, "Mine-A-Left");
		metroids.put(26, "Mine-G-Right");
		metroids.put(27, "A4-Z-Right side");
		metroids.put(28, "A4-G-Above first Zeta");
		metroids.put(29, "A4-G-Top right corner");
		metroids.put(30, "A4-Z-In cracked wall");
		metroids.put(31, "A4-G-Left side");
		metroids.put(32, "A4-Z-Lower left");
		metroids.put(33, "A5-Z-Middle of the Map");
		metroids.put(34, "A5-A-5 Alpha room A");
		metroids.put(35, "A5-A-5 Alpha room B");
		metroids.put(36, "A5-A-5 Alpha room C");
		metroids.put(37, "A5-A-5 Alpha room D");
		metroids.put(38, "A5-A-5 Alpha room E");
		metroids.put(39, "A5-G-2 Gamma room A");
		metroids.put(40, "A5-G-2 Gamma room B");
		metroids.put(41, "A6-A-Final Alpha ");
		metroids.put(42, "A6-O-First Omega");
		metroids.put(43, "A7-O-Top left");
		metroids.put(44, "A7-O-Right side");
		metroids.put(45, "A7-O-Bottom left");
		metroids.put(46, "A8-L-1 of 8");
		metroids.put(47, "A8-L-2 of 8");
		metroids.put(48, "A8-L-3 of 8");
		metroids.put(49, "A8-L-4 of 8");
		metroids.put(50, "A8-L-5 of 8");
		metroids.put(51, "A8-L-6 of 8");
		metroids.put(52, "A8-L-7 of 8");
		metroids.put(53, "A8-L-8 of 8");
		metroids.put(54, "A8-Q-Queen (has no effect if it's selected before final fight)");

		randomizer = new HashMap<>();
		randomizer.put(0, "mod_bombs");
		randomizer.put(1, "mod_spider");
		randomizer.put(2, "mod_jumpball");
		randomizer.put(3, "mod_hijump");
		randomizer.put(4, "mod_varia");
		randomizer.put(5, "mod_spacejump");
		randomizer.put(6, "mod_speedbooster");
		randomizer.put(7, "mod_screwattack");
		randomizer.put(8, "mod_charge");
		randomizer.put(9, "mod_ice");
		randomizer.put(10, "mod_wave");
		randomizer.put(11, "mod_spazer");
		randomizer.put(12, "mod_plasma");

		randomizer.put(13, "mod_52");
		randomizer.put(14, "mod_53");
		randomizer.put(15, "mod_54");
		randomizer.put(16, "mod_55");
		randomizer.put(17, "mod_56");
		randomizer.put(18, "mod_57");
		randomizer.put(19, "mod_60");
		randomizer.put(20, "mod_100");
		randomizer.put(21, "mod_101");
		randomizer.put(22, "mod_102");
		randomizer.put(23, "mod_104");
		randomizer.put(24, "mod_105");
		randomizer.put(25, "mod_106");
		randomizer.put(26, "mod_107");
		randomizer.put(27, "mod_109");
		randomizer.put(28, "mod_111");
		randomizer.put(29, "mod_150");
		randomizer.put(30, "mod_151");
		randomizer.put(31, "mod_152");
		randomizer.put(32, "mod_153");
		randomizer.put(33, "mod_154");
		randomizer.put(34, "mod_155");
		randomizer.put(35, "mod_156");
		randomizer.put(36, "mod_159");
		randomizer.put(37, "mod_161");
		randomizer.put(38, "mod_163");
		randomizer.put(39, "mod_202");
		randomizer.put(40, "mod_203");
		randomizer.put(41, "mod_204");
		randomizer.put(42, "mod_205");
		randomizer.put(43, "mod_208");
		randomizer.put(44, "mod_210");
		randomizer.put(45, "mod_211");
		randomizer.put(46, "mod_214");
		randomizer.put(47, "mod_250");
		randomizer.put(48, "mod_252");
		randomizer.put(49, "mod_255");
		randomizer.put(50, "mod_257");
		randomizer.put(51, "mod_259");
		randomizer.put(52, "mod_303");
		randomizer.put(53, "mod_304");
		randomizer.put(54, "mod_307");
		randomizer.put(55, "mod_308");
		randomizer.put(56, "mod_309");

		randomizer.put(57, "mod_51");
		randomizer.put(58, "mod_110");
		randomizer.put(59, "mod_162");
		randomizer.put(60, "mod_206");
		randomizer.put(61, "mod_207");
		randomizer.put(62, "mod_209");
		randomizer.put(63, "mod_215");
		randomizer.put(64, "mod_256");
		randomizer.put(65, "mod_300");
		randomizer.put(66, "mod_305");

		randomizer.put(67, "mod_50");
		randomizer.put(68, "mod_103");
		randomizer.put(69, "mod_108");
		randomizer.put(70, "mod_157");
		randomizer.put(71, "mod_158");
		randomizer.put(72, "mod_200");
		randomizer.put(73, "mod_201");
		randomizer.put(74, "mod_251");
		randomizer.put(75, "mod_254");
		randomizer.put(76, "mod_306");

		randomizer.put(77, "mod_58");
		randomizer.put(78, "mod_59");
		randomizer.put(79, "mod_112");
		randomizer.put(80, "mod_160");
		randomizer.put(81, "mod_212");
		randomizer.put(82, "mod_213");
		randomizer.put(83, "mod_253");
		randomizer.put(84, "mod_258");
		randomizer.put(85, "mod_301");
		randomizer.put(86, "mod_302");
	}

	public static String getEvent(int id) {
		String name = events.get(id);

		if(name != null)
			return name;
		else
			return String.format("event[%d]", id);
	}

	public static int[] getEventIDs() {
		int i = 0;
		int[] ids = new int[events.size()];
		for(int id : events.keySet())
			ids[i++] = id;
		Arrays.sort(ids);
		return ids;
	}

	public static String getItem(int id) {
		String name = items.get(id);

		if(name != null)
			return name;
		else
			return String.format("item[%d]", id);
	}

	public static int[] getItemIDs() {
		int i = 0;
		int[] ids = new int[items.size()];
		for(int id : items.keySet())
			ids[i++] = id;
		Arrays.sort(ids);
		return ids;
	}

	public static String getLog(int id) {
		String name = logs.get(id);

		if(name != null)
			return name;
		else
			return String.format("log[%d]", id);
	}

	public static String getTrooperLog(int id) {
		String name = trooperlogs.get(id);

		if(name != null)
			return name;
		else
			return String.format("trooperlog[%d]", id);
	}

	public static int[] getLogIDs() {
		int i = 0;
		int[] ids = new int[logs.size()];
		for(int id : logs.keySet())
			ids[i++] = id;
		Arrays.sort(ids);
		return ids;
	}

	public static String getHint(int id) {
		String name = hints.get(id);

		if(name != null)
			return name;
		else
			return String.format("hint[%d]", id);
	}

	public static int[] getHintIDs() {
		int i = 0;
		int[] ids = new int[hints.size()];
		for(int id : hints.keySet())
			ids[i++] = id;
		Arrays.sort(ids);
		return ids;
	}

	public static String getMetroid(int id) {
		String name = metroids.get(id);

		if(name != null)
			return name;
		else
			return String.format("metroid[%d]", id);
	}

	public static int[] getMetroidIDs() {
		int i = 0;
		int[] ids = new int[metroids.size()];
		for(int id : metroids.keySet())
			ids[i++] = id;
		Arrays.sort(ids);
		return ids;
	}

	public static String getRandomizer(int id) {
		String name = randomizer.get(id);

		if(name != null)
			return name;
		else
			return String.format("randomizer[%d]", id);
	}

	public static int[] getRandomizerIDs() {
		int i = 0;
		int[] ids = new int[randomizer.size()];
		for(int id : randomizer.keySet())
			ids[i++] = id;
		Arrays.sort(ids);
		return ids;
	}

	public static String getSaveStation(int id) {
		if(id >= 0 && id < SAVE_STATIONS.length)
			return SAVE_STATIONS[id] + " [" + SAVE_STATION_ROOMS[id] + "]";
		return Integer.toString(id);
	}
}
