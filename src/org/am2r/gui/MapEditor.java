package org.am2r.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.am2r.MapData;

public class MapEditor extends JPanel {
	private static final long serialVersionUID = 1L;

	private Model model;
	private JTable table;

	private class Model extends AbstractTableModel {
		private static final long serialVersionUID = 1L;

		private MapData data;

		public Model(MapData data) {
			this.data = data;
		}

		public void read(MapData data) {
			this.data = data;
			fireTableDataChanged();
		}

		@Override
		public int getColumnCount() {
			return 81;
		}

		@Override
		public int getRowCount() {
			return 80;
		}

		@Override
		public Object getValueAt(int row, int col) {
			if(col == 0)
				return String.valueOf(row);
			int x = col - 1;
			int y = row;
			return data.getMap(x, y);
		}

		@Override
		public void setValueAt(Object value, int row, int col) {
			if(col == 0)
				return;
			String val = String.valueOf(value);
			int x = col - 1;
			int y = row;
			try {
				data.setMap(x, y, Integer.parseInt(val));
			} catch(NumberFormatException e) {
			}
		}

		@Override
		public String getColumnName(int col) {
			return col == 0 ? "" : String.valueOf(col - 1);
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			return col > 0;
		}
	}

	public MapEditor(MapData data) {
		super(new BorderLayout());
		model = new Model(data);
		table = new MapTable(model);
		table.setPreferredScrollableViewportSize(table.getPreferredSize());
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		for(int i = 1; i <= 80; i++)
			table.getColumnModel().getColumn(i).setPreferredWidth(20);
		add(BorderLayout.CENTER, new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
	}

	public void read(MapData data) {
		model.read(data);
	}
}
