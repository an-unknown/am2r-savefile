package org.am2r.gui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

public class MapTable extends JTable {
	private static final long serialVersionUID = 1L;
	private Class<?> editingClass;

	public MapTable(AbstractTableModel model) {
		super(model);
	}

	private class Renderer implements TableCellRenderer {
		private TableCellRenderer parent;

		public Renderer(TableCellRenderer parent) {
			this.parent = parent;
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			Component c = parent.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
					column);
			if(column == 0)
				return c;
			else if(String.valueOf(value).equals("0"))
				c.setBackground(Color.LIGHT_GRAY);
			else if(String.valueOf(value).equals("1"))
				c.setBackground(Color.BLUE);
			else if(String.valueOf(value).equals("2"))
				c.setBackground(Color.GREEN);
			else if(String.valueOf(value).equals("10"))
				c.setBackground(Color.MAGENTA);
			else if(String.valueOf(value).equals("11"))
				c.setBackground(Color.RED);
			else
				c.setBackground(Color.LIGHT_GRAY);
			return c;
		}
	}

	@Override
	public TableCellRenderer getCellRenderer(int row, int column) {
		editingClass = null;
		int modelColumn = convertColumnIndexToModel(column);
		Object value = getModel().getValueAt(row, modelColumn);
		Class<?> rowClass = value != null ? value.getClass() : String.class;
		return new Renderer(getDefaultRenderer(rowClass));
	}

	@Override
	public TableCellEditor getCellEditor(int row, int column) {
		editingClass = null;
		int modelColumn = convertColumnIndexToModel(column);
		Object value = getModel().getValueAt(row, modelColumn);
		editingClass = value != null ? value.getClass() : String.class;
		return getDefaultEditor(editingClass);
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return editingClass != null ? editingClass : super.getColumnClass(column);
	}
}
