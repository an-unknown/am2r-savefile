package org.am2r.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.am2r.Save;

public class MainWindow {
	public static final String VERSION = "2.8";
	public static final String ABOUT_TEXT = "<html><body>AM2R Save File Editor v" + VERSION + "\nby unknown\n"
			+ "\nSave file templates by CraigKostelecky\n"
			+ "\nUpdates: <a href=\"http://am2r.freeforums.org/save-file-editor-t1803.html\">http://am2r.freeforums.org/save-file-editor-t1803.html</a>\n"
			+ "\nSource code: <a href=\"https://gitlab.com/an-unknown/am2r-savefile\">https://gitlab.com/an-unknown/am2r-savefile</a>"
			+ "</body></html>";

	private JFrame root;

	private Save save;
	private MainEditor mainEditor;
	private InventoryEditor inventoryEditor;
	private ItemEditor itemEditor;
	private EventEditor eventEditor;
	private LogEditor logEditor;
	private TrooperLogEditor trooperLogEditor;
	private HintEditor hintEditor;
	private MetroidEditor metroidEditor;
	private MapEditor mapEditor;
	private RandomizerEditor randomizerEditor;

	private JFileChooser fileChooser;

	public MainWindow() {
		fileChooser = new JFileChooser();
		if(System.getProperty("os.name").contains("Windows")) {
			String appdata = System.getenv("LOCALAPPDATA");
			if(appdata != null) {
				File path = new File(appdata + "/AM2R");
				if(path.exists())
					fileChooser.setCurrentDirectory(path);
			}
		}

		save = new Save();
		root = new JFrame("AM2R Save File Editor");
		JTabbedPane tabs = new JTabbedPane();
		tabs.addTab("Main", mainEditor = new MainEditor(save.getMain()));
		tabs.addTab("Inventory", inventoryEditor = new InventoryEditor(save.getInventory()));
		tabs.addTab("Items", itemEditor = new ItemEditor(save.getItems()));
		tabs.addTab("Events", eventEditor = new EventEditor(save.getEvents()));
		tabs.addTab("Logs", logEditor = new LogEditor(save.getLogs()));
		tabs.addTab("Trooper Logs", trooperLogEditor = new TrooperLogEditor(save.getTrooperLogs()));
		tabs.addTab("Hints", hintEditor = new HintEditor(save.getHints()));
		tabs.addTab("Metroids", metroidEditor = new MetroidEditor(save.getMetroids()));
		tabs.addTab("Map", mapEditor = new MapEditor(save.getMap()));
		tabs.addTab("Randomizer", randomizerEditor = new RandomizerEditor(save.getRandomizer()));
		root.setLayout(new BorderLayout());
		root.add(BorderLayout.CENTER, tabs);
		JMenuBar menu = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		menu.add(fileMenu);
		JMenuItem fileMenuLoad = new JMenuItem("Load...");
		JMenuItem fileMenuSave = new JMenuItem("Save...");
		JMenuItem fileMenuMerge = new JMenuItem("Merge...");
		JMenuItem fileMenuExit = new JMenuItem("Exit");
		fileMenuLoad.addActionListener((e) -> {
			int result = fileChooser.showOpenDialog(root);
			if(result != JFileChooser.APPROVE_OPTION)
				return;
			File file = fileChooser.getSelectedFile();
			try {
				read(file.toString());
			} catch(IOException ex) {
				showError(ex);
			}
		});
		fileMenuSave.addActionListener((e) -> {
			int result = fileChooser.showSaveDialog(root);
			if(result != JFileChooser.APPROVE_OPTION)
				return;
			File file = fileChooser.getSelectedFile();
			try {
				write(file.toString());
			} catch(IOException ex) {
				showError(ex);
			}
		});
		fileMenuMerge.addActionListener((e) -> {
			int result = fileChooser.showOpenDialog(root);
			if(result != JFileChooser.APPROVE_OPTION)
				return;
			File file = fileChooser.getSelectedFile();
			try {
				Save other = new Save(file.toString());
				MergeWindow merge = new MergeWindow(root, save, other);
				merge.show();
				read(save);
			} catch(IOException ex) {
				showError(ex);
			}
		});
		fileMenuExit.addActionListener((e) -> System.exit(0));
		fileMenuLoad.setMnemonic('L');
		fileMenuSave.setMnemonic('S');
		fileMenuMerge.setMnemonic('M');
		fileMenuExit.setMnemonic('x');
		fileMenu.add(fileMenuLoad);
		fileMenu.add(fileMenuSave);
		fileMenu.addSeparator();
		fileMenu.add(fileMenuMerge);
		fileMenu.addSeparator();
		fileMenu.add(fileMenuExit);
		fileMenu.setMnemonic('F');
		menu.add(fileMenu);
		JMenu templateMenu = new JMenu("Templates");
		JMenuItem templateMenuGamePlus = new JMenuItem("NewGame+");
		JMenuItem templateMenuAllItemsGamePlus = new JMenuItem("All Items: NewGame+");
		JMenuItem templateMenuM1 = new JMenuItem("Metroid I 100%");
		JMenuItem templateMenuZM = new JMenuItem("Metroid Zero Mission 100%");
		JMenuItem templateMenuEasy = new JMenuItem("New: Easy Mode");
		JMenuItem templateMenuNormal = new JMenuItem("New: Normal Mode");
		JMenuItem templateMenuHard = new JMenuItem("New: Hard Mode");
		JMenuItem templateMenuFullMap = new JMenuItem("Full Map");
		templateMenuGamePlus.addActionListener((e) -> template("NewGame+"));
		templateMenuGamePlus.setMnemonic('G');
		templateMenuAllItemsGamePlus.addActionListener((e) -> template("AllItems-NewGame+"));
		templateMenuAllItemsGamePlus.setMnemonic('A');
		templateMenuM1.addActionListener((e) -> template("M1-100p"));
		templateMenuM1.setMnemonic('M');
		templateMenuZM.addActionListener((e) -> template("ZM-100p"));
		templateMenuZM.setMnemonic('Z');
		templateMenuEasy.addActionListener((e) -> template("NewEasyGame"));
		templateMenuEasy.setMnemonic('E');
		templateMenuNormal.addActionListener((e) -> template("NewNormalGame"));
		templateMenuNormal.setMnemonic('N');
		templateMenuHard.addActionListener((e) -> template("NewHardGame"));
		templateMenuHard.setMnemonic('H');
		templateMenuFullMap.addActionListener((e) -> template("full-map"));
		templateMenuFullMap.setMnemonic('F');
		templateMenu.add(templateMenuGamePlus);
		templateMenu.add(templateMenuAllItemsGamePlus);
		templateMenu.addSeparator();
		templateMenu.add(templateMenuM1);
		templateMenu.add(templateMenuZM);
		templateMenu.addSeparator();
		templateMenu.add(templateMenuEasy);
		templateMenu.add(templateMenuNormal);
		templateMenu.add(templateMenuHard);
		templateMenu.addSeparator();
		templateMenu.add(templateMenuFullMap);
		templateMenu.setMnemonic('T');
		menu.add(templateMenu);
		JMenu helpMenu = new JMenu("Help");
		JMenuItem helpMenuAbout = new JMenuItem("About...");
		helpMenuAbout.addActionListener((e) -> showMessageDialog(root, ABOUT_TEXT, "About...",
				JOptionPane.INFORMATION_MESSAGE));
		helpMenuAbout.setMnemonic('A');
		helpMenu.add(helpMenuAbout);
		helpMenu.setMnemonic('H');
		menu.add(helpMenu);
		root.setJMenuBar(menu);
		root.setSize(300, 300);
		root.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void show() {
		root.setVisible(true);
	}

	private void template(String name) {
		try {
			readResource(name);
		} catch(IOException ex) {
			showError(ex);
		}
	}

	public void readResource(String name) throws IOException {
		try(InputStream in = getResource(name)) {
			read(in);
		}
	}

	private static InputStream getResource(String name) {
		return MainWindow.class.getResourceAsStream("/org/am2r/resources/" + name);
	}

	public void read(InputStream in) throws IOException {
		Save save = new Save(in);
		read(save);
	}

	public void read(String filename) throws IOException {
		Save save = new Save(filename);
		read(save);
	}

	private void read(Save save) {
		this.save = save;
		mainEditor.read(save.getMain());
		inventoryEditor.read(save.getInventory());
		itemEditor.read(save.getItems());
		eventEditor.read(save.getEvents());
		logEditor.read(save.getLogs());
		trooperLogEditor.read(save.getTrooperLogs());
		hintEditor.read(save.getHints());
		metroidEditor.read(save.getMetroids());
		mapEditor.read(save.getMap());
		randomizerEditor.read(save.getRandomizer());
	}

	public void write(String filename) throws IOException {
		save.write(filename);
	}

	private void showMessageDialog(Component root, String message, String title, int options) {
		// for copying style
		JLabel label = new JLabel();
		Font font = label.getFont();
		Color fgcolor = label.getForeground();
		Color bgcolor = label.getBackground();

		// create some css from the label's font
		StringBuffer style = new StringBuffer("font-family:" + font.getFamily() + ";");
		style.append("font-weight:" + (font.isBold() ? "bold" : "normal") + ";");
		style.append("font-size:" + font.getSize() + "pt;");
		style.append("color: rgb(" + fgcolor.getRed() + "," + fgcolor.getGreen() + "," + fgcolor.getBlue()
				+ ");");
		style.append("background-color: rgb(" + bgcolor.getRed() + "," + bgcolor.getGreen() + ","
				+ bgcolor.getBlue() + ");");
		JEditorPane ep = new JEditorPane("text/html",
				message.replace("<body>", "<body style=\"" + style + "\">").replace("\n", "<br/>"));
		ep.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if(e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
					Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
					if(desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
						try {
							desktop.browse(e.getURL().toURI());
						} catch(Exception ex) {
							ex.printStackTrace();
						}
					}
				}
			}
		});
		ep.setEditable(false);
		ep.setBorder(null);
		// ep.getCaret().deinstall(ep);
		JOptionPane.showMessageDialog(root, ep, title, options);
	}

	private void showError(Throwable t) {
		t.printStackTrace();
		String message;
		if(t.getMessage() != null) {
			message = t.getClass().getSimpleName() + ": " + t.getMessage();
		} else {
			message = t.toString();
		}
		message = "<html><body>" + message.replace("&", "&amp;").replace("<", "&lt;") + "</body></html>";
		showMessageDialog(root, message, t.getClass().getSimpleName(), JOptionPane.ERROR_MESSAGE);
	}

	public static void main(String[] args) {
		MainWindow main = new MainWindow();
		main.show();
	}
}
