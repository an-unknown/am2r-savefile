package org.am2r.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.am2r.LogData;

public class LogEditor extends JPanel {
	private static final long serialVersionUID = 1L;

	private static final int[] ids = Names.getLogIDs();

	private Model model;
	private JTable table;

	private class Model extends AbstractTableModel {
		private static final long serialVersionUID = 1L;

		private final String[] CAPTIONS = { "Name", "Value", "New" };

		private LogData data;

		public Model(LogData data) {
			this.data = data;
		}

		public void read(LogData data) {
			this.data = data;
			fireTableDataChanged();
		}

		@Override
		public int getColumnCount() {
			return 3;
		}

		@Override
		public int getRowCount() {
			return ids.length; // 50
		}

		@Override
		public Object getValueAt(int row, int col) {
			int id = ids[row];
			if(col == 0)
				return Names.getLog(id);
			else if(col == 1)
				return data.getLog(id);
			else
				return data.getNew(id);
		}

		@Override
		public void setValueAt(Object value, int row, int col) {
			if(col == 0)
				return;
			int id = ids[row];
			String val = String.valueOf(value);
			if(col == 1) {
				try {
					data.setLog(id, Integer.parseInt(val));
				} catch(NumberFormatException e) {
				}
			} else if(col == 2)
				data.setNew(id, Boolean.parseBoolean(val));
		}

		@Override
		public String getColumnName(int col) {
			return CAPTIONS[col];
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			return col > 0;
		}
	}

	public LogEditor(LogData data) {
		super(new BorderLayout());
		model = new Model(data);
		table = new MixedTable(model);
		add(BorderLayout.CENTER, new JScrollPane(table));
	}

	public void read(LogData data) {
		model.read(data);
	}
}
