package org.am2r.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.am2r.RandomizerData;

public class RandomizerEditor extends JPanel {
	private static final long serialVersionUID = 1L;

	private static final int[] ids = Names.getRandomizerIDs();

	private Model model;
	private JTable table;

	private class Model extends ExtendedTableModel {
		private static final long serialVersionUID = 1L;

		private final String[] CAPTIONS = { "Name", "Value" };

		private final String[] EXTRA_NAMES = { "Pure Random", "Random Game", "Split Random", "Fusion",
				"Diff Mult", "Chozo Message", "mod_gravity", "Gamemode" };

		private RandomizerData data;

		public Model(RandomizerData data) {
			this.data = data;
		}

		public void read(RandomizerData data) {
			this.data = data;
			fireTableDataChanged();
		}

		@Override
		public int getColumnCount() {
			return 2;
		}

		@Override
		public int getRowCount() {
			return ids.length + EXTRA_NAMES.length;
		}

		@Override
		public Object getValueAt(int row, int col) {
			if(row >= ids.length) {
				int off = row - ids.length;
				if(col == 0)
					return EXTRA_NAMES[off];
				switch(off) {
				case 0:
					return data.isPureRandom();
				case 1:
					return data.isRandomGame();
				case 2:
					return data.isSplitRandom();
				case 3:
					return data.isFusion();
				case 4:
					return data.getDiffMult();
				case 5:
					return data.getChozoMessage();
				case 6:
					return data.getModGravity();
				case 7:
					return data.getGameMode();
				default:
					throw new IllegalArgumentException();
				}
			} else {
				int id = ids[row];
				if(col == 0)
					return Names.getRandomizer(id);
				else
					return data.getValue(id);
			}
		}

		@Override
		public void setValueAt(Object value, int row, int col) {
			if(col == 0)
				return;
			if(row >= ids.length) {
				int off = row - ids.length;
				switch(off) {
				case 0:
					data.setPureRandom((Boolean) value);
					return;
				case 1:
					data.setRandomGame((Boolean) value);
					return;
				case 2:
					data.setSplitRandom((Boolean) value);
					return;
				case 3:
					data.setFusion((Boolean) value);
					return;
				case 4:
					data.setDiffMult((Double) value);
					return;
				case 5:
					data.setChozoMessage((Integer) value);
					return;
				case 6:
					data.setModGravity((Integer) value);
					return;
				case 7:
					data.setGameMode((Integer) value);
					return;
				default:
					throw new IllegalArgumentException();
				}
			} else {
				int id = ids[row];
				try {
					data.setValue(id, (Integer) value);
				} catch(NumberFormatException e) {
				}
			}
		}

		@Override
		public String getColumnName(int col) {
			return CAPTIONS[col];
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			return col > 0;
		}
	}

	public RandomizerEditor(RandomizerData data) {
		super(new BorderLayout());
		model = new Model(data);
		table = new MixedTable(model);
		add(BorderLayout.CENTER, new JScrollPane(table));
	}

	public void read(RandomizerData data) {
		model.read(data);
	}
}
