package org.am2r.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.am2r.MetroidData;

public class MetroidEditor extends JPanel {
	private static final long serialVersionUID = 1L;

	private static final int[] ids = Names.getMetroidIDs();

	private Model model;
	private JTable table;

	private class Model extends AbstractTableModel {
		private static final long serialVersionUID = 1L;

		private MetroidData data;

		public Model(MetroidData data) {
			this.data = data;
		}

		public void read(MetroidData data) {
			this.data = data;
			fireTableDataChanged();
		}

		@Override
		public int getColumnCount() {
			return 2;
		}

		@Override
		public int getRowCount() {
			return ids.length; // 100
		}

		@Override
		public Object getValueAt(int row, int col) {
			int id = ids[row];
			if(col == 0)
				return Names.getMetroid(id);
			return data.getMetroid(id);
		}

		@Override
		public void setValueAt(Object value, int row, int col) {
			if(col == 0)
				return;
			int id = ids[row];
			String val = String.valueOf(value);
			data.setMetroid(id, Boolean.parseBoolean(val));
		}

		@Override
		public String getColumnName(int col) {
			return col == 0 ? "Name" : "Value";
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			return col > 0;
		}
	}

	public MetroidEditor(MetroidData data) {
		super(new BorderLayout());
		model = new Model(data);
		table = new MixedTable(model);
		add(BorderLayout.CENTER, new JScrollPane(table));
	}

	public void read(MetroidData data) {
		model.read(data);
	}
}
