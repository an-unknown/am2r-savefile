package org.am2r.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.am2r.MainData;

public class MainEditor extends JPanel {
	private static final long serialVersionUID = 1L;

	private Model model;
	private JTable table;

	private class Model extends ExtendedTableModel {
		private static final long serialVersionUID = 1L;

		private final String[] CAPTIONS = { "Save Room", "Difficulty", "Game Time", "Items Taken",
				"Energy Tanks", "Missile Tanks", "Supermissile Tanks", "Powerbomb Tanks",
				"Metroids Left", "Metroids Area", "Lava State", "Map Marker", "Map Marker X",
				"Map Marker Y" };

		private MainData data;

		public Model(MainData data) {
			this.data = data;
		}

		public void read(MainData data) {
			this.data = data;
			fireTableDataChanged();
		}

		@Override
		public int getColumnCount() {
			return 2;
		}

		@Override
		public int getRowCount() {
			return CAPTIONS.length;
		}

		@Override
		public Object getDisplayValueAt(int row, int col) {
			if(col != 0) {
				if(row == 0)
					return Names.getSaveStation(data.getSaveRoom());
			}
			return getValueAt(row, col);
		}

		@Override
		public Object getValueAt(int row, int col) {
			if(col == 0)
				return CAPTIONS[row];
			switch(row) {
			case 0:
				return data.getSaveRoom();
			case 1:
				return data.getDifficulty();
			case 2:
				return data.getGameTime();
			case 3:
				return data.getItemsTaken();
			case 4:
				return data.getETanks();
			case 5:
				return data.getMTanks();
			case 6:
				return data.getSTanks();
			case 7:
				return data.getPTanks();
			case 8:
				return data.getMetroidsLeft();
			case 9:
				return data.getMetroidsArea();
			case 10:
				return data.getLavaState();
			case 11:
				return data.isMapMarker();
			case 12:
				return data.getMapMarkerX();
			case 13:
				return data.getMapMarkerY();
			}
			return null;
		}

		@Override
		public void setValueAt(Object value, int row, int col) {
			if(col == 0)
				return;
			String val = String.valueOf(value);
			try {
				switch(row) {
				case 0:
					data.setSaveRoom(Integer.parseInt(val));
					break;
				case 1:
					data.setDifficulty(Integer.parseInt(val));
					break;
				case 2:
					data.setGameTime(Double.parseDouble(val));
					break;
				case 3:
					data.setItemsTaken(Integer.parseInt(val));
					break;
				case 4:
					data.setETanks(Integer.parseInt(val));
					break;
				case 5:
					data.setMTanks(Integer.parseInt(val));
					break;
				case 6:
					data.setSTanks(Integer.parseInt(val));
					break;
				case 7:
					data.setPTanks(Integer.parseInt(val));
					break;
				case 8:
					data.setMetroidsLeft(Integer.parseInt(val));
					break;
				case 9:
					data.setMetroidsArea(Integer.parseInt(val));
					break;
				case 10:
					data.setLavaState(Integer.parseInt(val));
					break;
				case 11:
					data.setMapMarker(Boolean.parseBoolean(val));
					break;
				case 12:
					data.setMapMarkerX(Double.parseDouble(val));
					break;
				case 13:
					data.setMapMarkerY(Double.parseDouble(val));
					break;
				}
			} catch(NumberFormatException e) {
			}
		}

		@Override
		public String getColumnName(int col) {
			return col == 0 ? "Name" : "Value";
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			return col > 0;
		}
	}

	public MainEditor(MainData data) {
		super(new BorderLayout());
		model = new Model(data);
		table = new MixedTable(model);
		add(BorderLayout.CENTER, new JScrollPane(table));
	}

	public void read(MainData data) {
		model.read(data);
	}
}
