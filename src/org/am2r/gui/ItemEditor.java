package org.am2r.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.am2r.ItemData;

public class ItemEditor extends JPanel {
	private static final long serialVersionUID = 1L;

	private static final int[] ids = Names.getItemIDs();

	private Model model;
	private JTable table;

	private class Model extends AbstractTableModel {
		private static final long serialVersionUID = 1L;

		private ItemData data;

		public Model(ItemData data) {
			this.data = data;
		}

		public void read(ItemData data) {
			this.data = data;
			fireTableDataChanged();
		}

		@Override
		public int getColumnCount() {
			return 2;
		}

		@Override
		public int getRowCount() {
			return ids.length; // 350
		}

		@Override
		public Object getValueAt(int row, int col) {
			int id = ids[row];
			if(col == 0)
				return Names.getItem(id);
			return data.getItem(id);
		}

		@Override
		public void setValueAt(Object value, int row, int col) {
			if(col == 0)
				return;
			String val = String.valueOf(value);
			int id = ids[row];
			data.setItem(id, Boolean.parseBoolean(val));
		}

		@Override
		public String getColumnName(int col) {
			return col == 0 ? "Name" : "Value";
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			return col > 0;
		}
	}

	public ItemEditor(ItemData data) {
		super(new BorderLayout());
		model = new Model(data);
		table = new MixedTable(model);
		add(BorderLayout.CENTER, new JScrollPane(table));
	}

	public void read(ItemData data) {
		model.read(data);
	}
}
