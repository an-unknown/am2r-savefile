package org.am2r.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.am2r.InventoryData;

public class InventoryEditor extends JPanel {
	private static final long serialVersionUID = 1L;

	private Model model;
	private JTable table;

	private class Model extends AbstractTableModel {
		private static final long serialVersionUID = 1L;

		private final String[] CAPTIONS = { "Current Suit", "Charge Beam", "Ice Beam", "Wave Beam",
				"Plasma Beam", "Spazer Beam", "Morph Ball", "Jump Ball", "Power Grip", "Space Jump",
				"Screw Attack", "Hi-Jump", "Spider Ball", "Speed Booster", "Bomb" };

		private InventoryData inventory;

		public Model(InventoryData inventory) {
			this.inventory = inventory;
		}

		public void read(InventoryData inventory) {
			this.inventory = inventory;
			fireTableDataChanged();
		}

		@Override
		public int getColumnCount() {
			return 2;
		}

		@Override
		public int getRowCount() {
			return CAPTIONS.length;
		}

		@Override
		public Object getValueAt(int row, int col) {
			if(col == 0)
				return CAPTIONS[row];
			switch(row) {
			case 0:
				return inventory.getCurrentSuit();
			case 1:
				return inventory.isCbeam();
			case 2:
				return inventory.isIbeam();
			case 3:
				return inventory.isWbeam();
			case 4:
				return inventory.isPbeam();
			case 5:
				return inventory.isSbeam();
			case 6:
				return inventory.isMorphBall();
			case 7:
				return inventory.isJumpBall();
			case 8:
				return inventory.isPowerGrip();
			case 9:
				return inventory.isSpaceJump();
			case 10:
				return inventory.isScrewAttack();
			case 11:
				return inventory.isHiJump();
			case 12:
				return inventory.isSpiderBall();
			case 13:
				return inventory.isSpeedBooster();
			case 14:
				return inventory.isBomb();
			}
			return null;
		}

		@Override
		public void setValueAt(Object value, int row, int col) {
			if(col == 0)
				return;
			String val = String.valueOf(value);
			switch(row) {
			case 0:
				try {
					int v = Integer.parseInt(val);
					inventory.setCurrentSuit(v);
				} catch(NumberFormatException e) {
				}
				break;
			case 1:
				inventory.setCbeam(Boolean.parseBoolean(val));
				break;
			case 2:
				inventory.setIbeam(Boolean.parseBoolean(val));
				break;
			case 3:
				inventory.setWbeam(Boolean.parseBoolean(val));
				break;
			case 4:
				inventory.setPbeam(Boolean.parseBoolean(val));
				break;
			case 5:
				inventory.setSbeam(Boolean.parseBoolean(val));
				break;
			case 6:
				inventory.setMorphBall(Boolean.parseBoolean(val));
				break;
			case 7:
				inventory.setJumpBall(Boolean.parseBoolean(val));
				break;
			case 8:
				inventory.setPowerGrip(Boolean.parseBoolean(val));
				break;
			case 9:
				inventory.setSpaceJump(Boolean.parseBoolean(val));
				break;
			case 10:
				inventory.setScrewAttack(Boolean.parseBoolean(val));
				break;
			case 11:
				inventory.setHiJump(Boolean.parseBoolean(val));
				break;
			case 12:
				inventory.setSpiderBall(Boolean.parseBoolean(val));
				break;
			case 13:
				inventory.setSpeedBooster(Boolean.parseBoolean(val));
				break;
			case 14:
				inventory.setBomb(Boolean.parseBoolean(val));
				break;
			}
		}

		@Override
		public String getColumnName(int col) {
			return col == 0 ? "Name" : "Value";
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			return col > 0;
		}
	}

	public InventoryEditor(InventoryData inventory) {
		super(new BorderLayout());
		model = new Model(inventory);
		table = new MixedTable(model);
		add(BorderLayout.CENTER, new JScrollPane(table));
	}

	public void read(InventoryData inventory) {
		model.read(inventory);
	}
}
