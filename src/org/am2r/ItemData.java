package org.am2r;

import java.io.IOException;

public class ItemData {
	private boolean[] items;

	public ItemData() {
		items = new boolean[350];
	}

	public ItemData(DsList list) {
		read(list);
	}

	public ItemData(String data) throws IOException {
		this(new DsList(data));
	}

	public void read(DsList list) {
		items = new boolean[350];
		for(int i = 0; i < items.length; i++)
			items[i] = list.getDouble(i) != 0.0;
	}

	public DsList write() {
		DsList list = new DsList(items.length);
		for(int i = 0; i < items.length; i++)
			list.setDouble(i, items[i] ? 1.0 : 0.0);
		return list;
	}

	public void set(ItemData other) {
		for(int i = 0; i < items.length; i++)
			setItem(i, other.getItem(i));
	}

	public boolean getItem(int i) {
		return items[i];
	}

	public void setItem(int i, boolean item) {
		items[i] = item;
	}

	public int size() {
		return items.length;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder("ItemData[");
		for(int i = 0; i <  size(); i++) {
			if(i > 0)
				b.append(',');
			b.append(getItem(i) ? '1' : '0');
		}
		return b.append("]").toString();
	}
}
