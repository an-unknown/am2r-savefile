package org.am2r;

import java.io.IOException;

public class InventoryData {
	private int currentSuit;
	private boolean cbeam;
	private boolean ibeam;
	private boolean wbeam;
	private boolean pbeam;
	private boolean sbeam;
	private boolean morphBall;
	private boolean jumpBall;
	private boolean powerGrip;
	private boolean spaceJump;
	private boolean screwAttack;
	private boolean hiJump;
	private boolean spiderBall;
	private boolean speedBooster;
	private boolean bomb;

	public InventoryData() {
	}

	public InventoryData(DsList data) {
		read(data);
	}

	public InventoryData(String data) throws IOException {
		this(new DsList(data));
	}

	public void read(DsList list) {
		currentSuit = (int) list.getDouble(0);
		cbeam = list.getDouble(1) != 0.0;
		ibeam = list.getDouble(2) != 0.0;
		wbeam = list.getDouble(3) != 0.0;
		pbeam = list.getDouble(4) != 0.0;
		sbeam = list.getDouble(5) != 0.0;
		morphBall = list.getDouble(6) != 0.0;
		jumpBall = list.getDouble(7) != 0.0;
		powerGrip = list.getDouble(8) != 0.0;
		spaceJump = list.getDouble(9) != 0.0;
		screwAttack = list.getDouble(10) != 0.0;
		hiJump = list.getDouble(11) != 0.0;
		spiderBall = list.getDouble(12) != 0.0;
		speedBooster = list.getDouble(13) != 0.0;
		bomb = list.getDouble(14) != 0.0;
	}

	public DsList write() {
		DsList list = new DsList(50);
		list.setDouble(0, currentSuit);
		list.setDouble(1, cbeam ? 1 : 0);
		list.setDouble(2, ibeam ? 1 : 0);
		list.setDouble(3, wbeam ? 1 : 0);
		list.setDouble(4, pbeam ? 1 : 0);
		list.setDouble(5, sbeam ? 1 : 0);
		list.setDouble(6, morphBall ? 1 : 0);
		list.setDouble(7, jumpBall ? 1 : 0);
		list.setDouble(8, powerGrip ? 1 : 0);
		list.setDouble(9, spaceJump ? 1 : 0);
		list.setDouble(10, screwAttack ? 1 : 0);
		list.setDouble(11, hiJump ? 1 : 0);
		list.setDouble(12, spiderBall ? 1 : 0);
		list.setDouble(13, speedBooster ? 1 : 0);
		list.setDouble(14, bomb ? 1 : 0);
		for(int i = 15; i < 50; i++)
			list.setDouble(i, 0);
		return list;
	}

	public void set(InventoryData other) {
		setCurrentSuit(other.getCurrentSuit());
		setCbeam(other.isCbeam());
		setIbeam(other.isIbeam());
		setWbeam(other.isWbeam());
		setPbeam(other.isPbeam());
		setSbeam(other.isSbeam());
		setMorphBall(other.isMorphBall());
		setJumpBall(other.isJumpBall());
		setPowerGrip(other.isPowerGrip());
		setSpaceJump(other.isSpaceJump());
		setScrewAttack(other.isScrewAttack());
		setHiJump(other.isHiJump());
		setSpiderBall(other.isSpiderBall());
		setSpeedBooster(other.isSpeedBooster());
		setBomb(other.isBomb());
	}

	public int getCurrentSuit() {
		return currentSuit;
	}

	public void setCurrentSuit(int currentSuit) {
		this.currentSuit = currentSuit;
	}

	public boolean isCbeam() {
		return cbeam;
	}

	public void setCbeam(boolean cbeam) {
		this.cbeam = cbeam;
	}

	public boolean isIbeam() {
		return ibeam;
	}

	public void setIbeam(boolean ibeam) {
		this.ibeam = ibeam;
	}

	public boolean isWbeam() {
		return wbeam;
	}

	public void setWbeam(boolean wbeam) {
		this.wbeam = wbeam;
	}

	public boolean isPbeam() {
		return pbeam;
	}

	public void setPbeam(boolean pbeam) {
		this.pbeam = pbeam;
	}

	public boolean isSbeam() {
		return sbeam;
	}

	public void setSbeam(boolean sbeam) {
		this.sbeam = sbeam;
	}

	public boolean isMorphBall() {
		return morphBall;
	}

	public void setMorphBall(boolean morphBall) {
		this.morphBall = morphBall;
	}

	public boolean isJumpBall() {
		return jumpBall;
	}

	public void setJumpBall(boolean jumpBall) {
		this.jumpBall = jumpBall;
	}

	public boolean isPowerGrip() {
		return powerGrip;
	}

	public void setPowerGrip(boolean powerGrip) {
		this.powerGrip = powerGrip;
	}

	public boolean isSpaceJump() {
		return spaceJump;
	}

	public void setSpaceJump(boolean spaceJump) {
		this.spaceJump = spaceJump;
	}

	public boolean isScrewAttack() {
		return screwAttack;
	}

	public void setScrewAttack(boolean screwAttack) {
		this.screwAttack = screwAttack;
	}

	public boolean isHiJump() {
		return hiJump;
	}

	public void setHiJump(boolean hiJump) {
		this.hiJump = hiJump;
	}

	public boolean isSpiderBall() {
		return spiderBall;
	}

	public void setSpiderBall(boolean spiderBall) {
		this.spiderBall = spiderBall;
	}

	public boolean isSpeedBooster() {
		return speedBooster;
	}

	public void setSpeedBooster(boolean speedBooster) {
		this.speedBooster = speedBooster;
	}

	public boolean isBomb() {
		return bomb;
	}

	public void setBomb(boolean bomb) {
		this.bomb = bomb;
	}

	@Override
	public String toString() {
		return "InventoryData[currentSuit=" + currentSuit
			+ ";cbeam=" + (cbeam ? '1' : '0')
			+ ";ibeam=" + (ibeam ? '1' : '0')
			+ ";wbeam=" + (wbeam ? '1' : '0')
			+ ";pbeam=" + (pbeam ? '1' : '0')
			+ ";sbeam=" + (sbeam ? '1' : '0')
			+ ";morphBall=" + (morphBall ? '1' : '0')
			+ ";jumpBall=" + (jumpBall ? '1' : '0')
			+ ";powerGrip=" + (powerGrip ? '1' : '0')
			+ ";spaceJump=" + (spaceJump ? '1' : '0')
			+ ";screwAttack=" + (screwAttack ? '1' : '0')
			+ ";hiJump=" + (hiJump ? '1' : '0')
			+ ";spiderBall=" + (spiderBall ? '1' : '0')
			+ ";speedBooster=" + (speedBooster ? '1' : '0')
			+ ";bomb=" + (bomb ? '1' : '0')
		       	+ "]";
	}
}
