package org.am2r;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

public class Save {
	public static final String KEY = "XOR_DFJykQ8xX3PuNnkLt6QviqALOLF8cxIDx1D63DAdph4KGQ4rOJ7";
	public static final byte[] MAGIC = utf8(rc4("[AM2R SaveData V7.0]", "HEADER_KEY"));

	private MainData main;
	private ItemData items;
	private EventData events;
	private LogData logs;
	private HintData hints;
	private MetroidData metroids;
	private InventoryData inventory;
	private MapData map;
	private RandomizerData randomizer;
	private TrooperLogData trooperLogs;

	private String[] extradata;

	public Save() {
		main = new MainData();
		items = new ItemData();
		events = new EventData();
		logs = new LogData();
		hints = new HintData();
		metroids = new MetroidData();
		inventory = new InventoryData();
		map = new MapData();
		randomizer = new RandomizerData();
		trooperLogs = new TrooperLogData();
	}

	public Save(String filename) throws IOException {
		String[] data = read(filename);
		read(data);
	}

	public Save(InputStream in) throws IOException {
		String[] data = read(in);
		read(data);
	}

	private static byte[] rc4(String str, String key) {
		return new RC4(key.getBytes()).encrypt(str.getBytes());
	}

	private static byte[] utf8(byte[] in) {
		char[] tmp = new char[in.length];
		for(int i = 0; i < in.length; i++) {
			tmp[i] = (char) (in[i] & 0xFF);
		}
		return new String(tmp).getBytes(StandardCharsets.UTF_8);
	}

	private void read(String[] data) throws IOException {
		main = new MainData(data[0]);
		items = new ItemData(data[1]);
		events = new EventData(data[2]);
		logs = new LogData(data[3]);
		hints = new HintData(data[4]);
		metroids = new MetroidData(data[5]);
		inventory = new InventoryData(data[6]);
		map = new MapData(data[7]);
		if(data.length > 8)
			randomizer = new RandomizerData(data[8]);
		else
			randomizer = new RandomizerData();
		if(data.length > 9)
			trooperLogs = new TrooperLogData(data[9]);
		else
			trooperLogs = new TrooperLogData();
		if(data.length > 10) {
			extradata = new String[data.length - 10];
			System.arraycopy(data, 10, extradata, 0, extradata.length);
		}
	}

	private String[] write() {
		int extra = extradata != null ? extradata.length : 0;
		String[] lines = new String[10 + extra];
		if(extra > 0)
			System.arraycopy(extradata, 0, lines, 10, extra);
		lines[0] = main.write().write();
		lines[1] = items.write().write();
		lines[2] = events.write().write();
		lines[3] = logs.write().write();
		lines[4] = hints.write().write();
		lines[5] = metroids.write().write();
		lines[6] = inventory.write().write();
		lines[7] = map.write().write();
		lines[8] = randomizer.write().write();
		lines[9] = trooperLogs.write().write();
		return lines;
	}

	public void write(OutputStream out) throws IOException {
		String[] lines = write();
		write(out, lines);
	}

	public void write(String filename) throws IOException {
		String[] lines = write();
		write(filename, lines);
	}

	public MainData getMain() {
		return main;
	}

	public ItemData getItems() {
		return items;
	}

	public EventData getEvents() {
		return events;
	}

	public LogData getLogs() {
		return logs;
	}

	public TrooperLogData getTrooperLogs() {
		return trooperLogs;
	}

	public HintData getHints() {
		return hints;
	}

	public MetroidData getMetroids() {
		return metroids;
	}

	public InventoryData getInventory() {
		return inventory;
	}

	public MapData getMap() {
		return map;
	}

	public RandomizerData getRandomizer() {
		return randomizer;
	}

	public static String[] read(InputStream in) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buf = new byte[256];
		int nbytes;
		while((nbytes = in.read(buf)) != -1)
			bos.write(buf, 0, nbytes);
		in.close();
		bos.close();
		buf = bos.toByteArray();

		byte[] data = Crypt.crypt(buf, KEY, 2);

		String[] lines = new String(data).split("\r\n");
		for(int i = 0; i < MAGIC.length; i++)
			if(data[i] != MAGIC[i])
				throw new IOException("save file corrupt: " + i);
		String[] out = new String[lines.length - 1];
		for(int i = 0; i < out.length; i++)
			out[i] = new String(Base64.decode(lines[i + 1]));
		return out;
	}

	public static String[] read(String fin) throws IOException {
		try(InputStream in = new FileInputStream(fin)) {
			return read(in);
		}
	}

	public static void write(String fout, String[] lines) throws IOException {
		try(OutputStream out = new FileOutputStream(fout)) {
			write(out, lines);
		}
	}

	public static void write(OutputStream out, String[] lines) throws IOException {
		String[] l = new String[lines.length];
		for(int i = 0; i < lines.length; i++)
			l[i] = Base64.encode(lines[i].getBytes());
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try(PrintStream ps = new PrintStream(bos)) {
			ps.write(MAGIC);
			ps.print("\r\n");
			for(String line : l) {
				ps.print(line);
				ps.print("\r\n");
			}
		}
		byte[] buf = bos.toByteArray();
		byte[] data = Crypt.crypt(buf, KEY, 2);
		out.write(data);
	}
}
