package org.am2r;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DsList {
	public static final int MAGIC = 0x12D;
	public static final int MAGIC_ALT = 0x12E;

	private Object[] elements;

	private static byte[] parseString(String s) {
		assert s.length() % 2 == 0;
		byte[] r = new byte[s.length() / 2];
		for(int i = 0; i < r.length; i++)
			r[i] = (byte) Integer.parseInt(s.substring(i * 2, i * 2 + 2), 16);
		return r;
	}

	private static String toHex(byte[] bytes) {
		StringBuilder buf = new StringBuilder();
		for(byte b : bytes)
			buf.append(String.format("%02X", b & 0xFF));
		return buf.toString();
	}

	public DsList(int size) {
		elements = new Object[size];
	}

	public DsList(String hex) throws IOException {
		assert hex.length() % 2 == 0;
		byte[] data = parseString(hex);
		try(LEInputStream in = new LEInputStream(new ByteArrayInputStream(data))) {
			int magic = in.read32bit();
			if(magic != MAGIC && magic != MAGIC_ALT)
				throw new IOException("invalid magic");
			int size = in.read32bit();
			assert size >= 0;
			elements = new Object[size];
			for(int i = 0; i < size; i++) {
				int type = in.read32bit();
				switch(type) {
				case 0:
				case 13: {
					long value = in.read64bit();
					elements[i] = Double.longBitsToDouble(value);
					break;
				}
				default:
					throw new IOException("unknown type: " + type + " (element " + i + " of " + size
							+ " elements)");
				}
			}
			assert in.available() == 0;
			assert in.tell() == data.length;
		}
	}

	public String write() {
		return write(false);
	}

	public String write(boolean altmagic) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try(LEOutputStream out = new LEOutputStream(bos)) {
			out.write32bit(altmagic ? MAGIC_ALT : MAGIC);
			out.write32bit(elements.length);
			for(Object o : elements) {
				if(o instanceof Double) {
					out.write32bit(0);
					out.write64bit((double) o);
				} else {
					throw new IllegalArgumentException("unsupported type: " + o.getClass());
				}
			}
			return toHex(bos.toByteArray());
		} catch(IOException e) {
			// cannot happen
			return null;
		}
	}

	public double getDouble(int i) {
		return (double) elements[i];
	}

	public void setDouble(int i, double d) {
		elements[i] = d;
	}

	public int size() {
		return elements.length;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder("DsList[size=").append(elements.length).append(";data=");
		if(elements.length > 0)
			b.append(elements[0]);
		for(int i = 1; i < elements.length; i++)
			b.append(", ").append(elements[i]);
		return b.append("]").toString();
	}
}
