package org.am2r;

import java.io.IOException;

public class EventData {
	private double[] events;

	public EventData() {
		events = new double[350];
	}

	public EventData(DsList list) {
		read(list);
	}

	public EventData(String data) throws IOException {
		this(new DsList(data));
	}

	public void read(DsList list) {
		events = new double[350];
		for(int i = 0; i < events.length; i++)
			events[i] = list.getDouble(i);
	}

	public DsList write() {
		DsList list = new DsList(events.length);
		for(int i = 0; i < events.length; i++)
			list.setDouble(i, events[i]);
		return list;
	}

	public void set(EventData other) {
		for(int i = 0; i < events.length; i++)
			setEvent(i, other.getEvent(i));
	}

	public double getEvent(int i) {
		return events[i];
	}

	public void setEvent(int i, double event) {
		events[i] = event;
	}

	public int size() {
		return events.length;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder("EventData[");
		for(int i = 0; i <  size(); i++) {
			if(i > 0)
				b.append(',');
			b.append(getEvent(i));
		}
		return b.append("]").toString();
	}
}
