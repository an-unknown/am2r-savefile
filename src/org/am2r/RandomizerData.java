package org.am2r;

import java.io.IOException;

public class RandomizerData {
	private int[] data;
	private double diffmult;

	private static final int[] DEFAULT_DATA = {
			// items
			0, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14,
			// missiles
			52, 53, 54, 55, 56, 57, 60, 100, 101, 102, 104, 105,
			106, 107, 109, 111, 150, 151, 152, 153, 154, 155, 156,
			159, 161, 163, 202, 203, 204, 205, 208, 210, 211, 214,
			250, 252, 255, 257, 259, 303, 304, 307, 308, 309,
			// super missiles
			51, 110, 162, 206, 207, 209, 215, 256, 300, 305,
			// energy tanks
			50, 103, 108, 157, 158, 200, 201, 251, 254, 306,
			// power bombs
			58, 59, 112, 160, 212, 213, 253, 258, 301, 302
	};

	public RandomizerData() {
		data = new int[120];
		System.arraycopy(DEFAULT_DATA, 0, data, 0, DEFAULT_DATA.length);
		setDiffMult(1);
		setChozoMessage(0);
		setModGravity(9);
		setGameMode(1);
	}

	public RandomizerData(DsList list) {
		read(list);
	}

	public RandomizerData(String data) throws IOException {
		this(new DsList(data));
	}

	public void read(DsList list) {
		data = new int[120];
		for(int i = 0; i < data.length; i++)
			data[i] = (int) list.getDouble(i);
		diffmult = list.getDouble(91);
	}

	public DsList write() {
		DsList list = new DsList(data.length);
		for(int i = 0; i < data.length; i++)
			list.setDouble(i, data[i]);
		list.setDouble(91, diffmult);
		return list;
	}

	public void set(RandomizerData other) {
		for(int i = 0; i < data.length; i++)
			setValue(i, other.getValue(i));
	}

	public boolean isPureRandom() {
		return data[87] != 0;
	}

	public void setPureRandom(boolean value) {
		data[87] = value ? 1 : 0;
	}

	public boolean isRandomGame() {
		return data[88] != 0;
	}

	public void setRandomGame(boolean value) {
		data[88] = value ? 1 : 0;
	}

	public boolean isSplitRandom() {
		return data[89] != 0;
	}

	public void setSplitRandom(boolean value) {
		data[89] = value ? 1 : 0;
	}

	public boolean isFusion() {
		return data[90] != 0;
	}

	public void setFusion(boolean value) {
		data[90] = value ? 1 : 0;
	}

	public double getDiffMult() {
		return diffmult;
	}

	public void setDiffMult(double value) {
		diffmult = value;
	}

	public int getChozoMessage() {
		return data[92];
	}

	public void setChozoMessage(int value) {
		data[92] = value;
	}

	public int getModGravity() {
		return data[93];
	}

	public void setModGravity(int value) {
		data[93] = value;
	}

	public int getGameMode() {
		return data[94];
	}

	public void setGameMode(int mode) {
		data[94] = mode;
	}

	public int getValue(int i) {
		return data[i];
	}

	public void setValue(int i, int entry) {
		data[i] = entry;
	}

	public int size() {
		return data.length;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder("RandomizerData[");
		for(int i = 0; i < size(); i++) {
			if(i > 0)
				b.append(',');
			b.append(getValue(i));
		}
		return b.append("]").toString();
	}
}
