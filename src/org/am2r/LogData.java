package org.am2r;

import java.io.IOException;

public class LogData {
	private int[] logs;
	private boolean[] newLogs;

	public LogData() {
		logs = new int[50];
		newLogs = new boolean[50];
	}

	public LogData(DsList list) {
		read(list);
	}

	public LogData(String data) throws IOException {
		this(new DsList(data));
	}

	public void read(DsList list) {
		logs = new int[50];
		newLogs = new boolean[50];
		int j = 0;
		for(int i = 0; i < logs.length; i++) {
			logs[i] = (int) list.getDouble(j++);
			newLogs[i] = list.getDouble(j++) != 0.0;
		}
	}

	public DsList write() {
		DsList list = new DsList(logs.length * 2);
		int j = 0;
		for(int i = 0; i < logs.length; i++) {
			list.setDouble(j++, logs[i]);
			list.setDouble(j++, newLogs[i] ? 1.0 : 0.0);
		}
		return list;
	}

	public void set(LogData other) {
		for(int i = 0; i < logs.length; i++) {
			setLog(i, other.getLog(i));
			setNew(i, other.getNew(i));
		}
	}

	public int getLog(int i) {
		return logs[i];
	}

	public void setLog(int i, int log) {
		logs[i] = log;
	}

	public boolean getNew(int i) {
		return newLogs[i];
	}

	public void setNew(int i, boolean newLog) {
		newLogs[i] = newLog;
	}
	public int size() {
		return logs.length;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder("LogData[");
		for(int i = 0; i <  size(); i++) {
			if(i > 0)
				b.append(',');
			b.append(getLog(i));
			b.append(':');
			b.append(getNew(i) ? '1' : '0');
		}
		return b.append("]").toString();
	}
}
