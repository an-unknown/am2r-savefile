package org.am2r;

import java.io.IOException;

public class TrooperLogData {
	private int[] logs;

	public TrooperLogData() {
		logs = new int[8];
	}

	public TrooperLogData(DsList list) {
		read(list);
	}

	public TrooperLogData(String data) throws IOException {
		this(new DsList(data));
	}

	public void read(DsList list) {
		logs = new int[8];
		for(int i = 0; i < logs.length; i++)
			logs[i] = (int) list.getDouble(i);
	}

	public DsList write() {
		DsList list = new DsList(logs.length);
		for(int i = 0; i < logs.length; i++)
			list.setDouble(i, logs[i]);
		return list;
	}

	public void set(TrooperLogData other) {
		for(int i = 0; i < logs.length; i++)
			setLog(i, other.getLog(i));
	}

	public int getLog(int i) {
		return logs[i];
	}

	public void setLog(int i, int log) {
		logs[i] = log;
	}

	public int size() {
		return logs.length;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder("TrooperLogData[");
		for(int i = 0; i < size(); i++) {
			if(i > 0)
				b.append(',');
			b.append(getLog(i));
		}
		return b.append("]").toString();
	}
}
