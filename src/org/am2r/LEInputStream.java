package org.am2r;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

public class LEInputStream extends InputStream {
	private InputStream parent;
	private boolean eof = false;
	private boolean debug = false;
	private long offset = 0;

	public LEInputStream(InputStream parent) {
		this.parent = parent;
	}

	public LEInputStream(InputStream parent, long offset) {
		this.parent = parent;
		this.offset = offset;
	}

	@Override
	public void close() throws IOException {
		parent.close();
	}

	@Override
	public int available() throws IOException {
		return parent.available();
	}

	@Override
	public long skip(long n) throws IOException {
		offset += n;
		return parent.skip(n);
	}

	@Override
	public void mark(int readlimit) {
		parent.mark(readlimit);
	}

	@Override
	public void reset() throws IOException {
		parent.reset();
	}

	@Override
	public boolean markSupported() {
		return parent.markSupported();
	}

	@Override
	public int read() throws IOException {
		if(eof)
			throw new EOFException();
		int result = parent.read();
		if(result == -1) {
			eof = true;
			throw new EOFException();
		}
		offset++;
		return result;
	}

	@Override
	public int read(byte[] buffer) throws IOException {
		if(eof)
			throw new EOFException();
		int result = parent.read(buffer);
		if(result == -1) {
			eof = true;
			throw new EOFException();
		}
		offset += result;
		return result;
	}

	@Override
	public int read(byte[] buffer, int offset, int length)
			throws IOException {
		if(eof)
			throw new EOFException();
		int result = parent.read(buffer, offset, length);
		if(result == -1) {
			eof = true;
			throw new EOFException();
		}
		offset += result;
		return result;
	}

	public int read8bit() throws IOException {
		if(debug) {
			int r = read();
			System.out.println("u8: " + r + " (s8: " + (byte) r
					+ "; bin: " + Integer.toString(r, 2)
					+ ")");
			return r;
		}
		return read();
	}

	public int read16bit() throws IOException {
		byte[] buf = new byte[2];
		read(buf);
		if(debug) {
			int r = Endianess.get16bit_LE(buf);
			System.out.println("u16: " + r + " (s16: " + (short) r
					+ "; bin: " + Integer.toString(r, 2)
					+ ")");
			return r;
		}
		return Endianess.get16bit_LE(buf);
	}

	public int read32bit() throws IOException {
		byte[] buf = new byte[4];
		read(buf);
		if(debug) {
			int r = Endianess.get32bit_LE(buf);
			System.out.println("u32: " + Integer.toUnsignedString(r)
					+ " (s32: " + r + "; bin: "
					+ Integer.toUnsignedString(r, 2) + ")");
			return r;
		}
		return Endianess.get32bit_LE(buf);
	}

	public long read64bit() throws IOException {
		byte[] buf = new byte[8];
		read(buf);
		if(debug) {
			long r = Endianess.get64bit_LE(buf);
			System.out.println("u64: " + Long.toUnsignedString(r)
					+ " (s64: " + r + "; bin: "
					+ Long.toUnsignedString(r, 2) + ")");
			return r;
		}
		return Endianess.get64bit_LE(buf);
	}

	public long tell() {
		return offset;
	}

	public void debug() {
		this.debug = true;
	}

	public boolean isDebug() {
		return debug;
	}
}
