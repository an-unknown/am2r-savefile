package org.am2r;

import java.io.IOException;

public class MetroidData {
	private boolean[] metroids;

	public MetroidData() {
		metroids = new boolean[100];
	}

	public MetroidData(DsList list) {
		read(list);
	}

	public MetroidData(String data) throws IOException {
		this(new DsList(data));
	}

	public void read(DsList list) {
		metroids = new boolean[100];
		for(int i = 0; i < metroids.length; i++)
			metroids[i] = list.getDouble(i) != 0.0;
	}

	public DsList write() {
		DsList list = new DsList(metroids.length);
		for(int i = 0; i < metroids.length; i++)
			list.setDouble(i, metroids[i] ? 1.0 : 0.0);
		return list;
	}

	public void set(MetroidData other) {
		for(int i = 0; i < metroids.length; i++)
			setMetroid(i, other.getMetroid(i));
	}

	public boolean getMetroid(int i) {
		return metroids[i];
	}

	public void setMetroid(int i, boolean metroid) {
		metroids[i] = metroid;
	}

	public int size() {
		return metroids.length;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder("MetroidData[");
		for(int i = 0; i <  size(); i++) {
			if(i > 0)
				b.append(',');
			b.append(getMetroid(i) ? '1' : '0');
		}
		return b.append("]").toString();
	}
}
