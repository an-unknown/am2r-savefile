package org.am2r;

import java.io.IOException;

public class MapData {
	public static final int UNVISITED = 0;
	public static final int VIISITED = 1;
	public static final int ITEM_COLLECTED = 2;

	private int[][] map;

	public MapData() {
		map = new int[80][80];
	}

	public MapData(DsList data) {
		read(data);
	}

	public MapData(String data) throws IOException {
		this(new DsList(data));
	}

	public void read(DsList list) {
		map = new int[80][80];
		int i = 0;
		for(int x = 0; x < 80; x++)
			for(int y = 0; y < 80; y++)
				map[y][x] = (int) list.getDouble(i++);
	}

	public DsList write() {
		DsList list = new DsList(80 * 80);
		int i = 0;
		for(int x = 0; x < 80; x++)
			for(int y = 0; y < 80; y++)
				list.setDouble(i++, map[y][x]);
		return list;
	}

	public void set(MapData other) {
		for(int y = 0; y < map.length; y++)
			for(int x = 0; x < map[y].length; x++)
				setMap(x, y, other.getMap(x, y));
	}

	public int getMap(int x, int y) {
		return map[y][x];
	}

	public void setMap(int x, int y, int value) {
		map[y][x] = value;
	}

	@Override
	public String toString() {
		return "MapData[80x80]";
	}
}
