package org.am2r;

import java.io.IOException;

public class MainData {
	private int saveRoom;
	private int difficulty;
	private double gameTime;
	private int itemsTaken;
	private int etanks;
	private int mtanks;
	private int stanks;
	private int ptanks;
	private int metroidsLeft;
	private int metroidsArea;
	private int lavaState;
	private boolean mapMarker;
	private double mapMarkerX;
	private double mapMarkerY;

	public MainData() {
	}

	public MainData(DsList data) {
		read(data);
	}

	public MainData(String data) throws IOException {
		this(new DsList(data));
	}

	public void read(DsList list) {
		saveRoom = (int) list.getDouble(0);
		difficulty = (int) list.getDouble(1);
		gameTime = list.getDouble(2);
		itemsTaken = (int) list.getDouble(3);
		etanks = (int) list.getDouble(4);
		mtanks = (int) list.getDouble(5);
		stanks = (int) list.getDouble(6);
		ptanks = (int) list.getDouble(7);
		metroidsLeft = (int) list.getDouble(8);
		metroidsArea = (int) list.getDouble(9);
		lavaState = (int) list.getDouble(10);
		mapMarker = list.getDouble(11) != 0.0;
		mapMarkerX = list.getDouble(12);
		mapMarkerY = list.getDouble(13);
	}

	public DsList write() {
		DsList list = new DsList(50);
		list.setDouble(0, saveRoom);
		list.setDouble(1, difficulty);
		list.setDouble(2, gameTime);
		list.setDouble(3, itemsTaken);
		list.setDouble(4, etanks);
		list.setDouble(5, mtanks);
		list.setDouble(6, stanks);
		list.setDouble(7, ptanks);
		list.setDouble(8, metroidsLeft);
		list.setDouble(9, metroidsArea);
		list.setDouble(10, lavaState);
		list.setDouble(11, mapMarker ? 1 : 0);
		list.setDouble(12, mapMarkerX);
		list.setDouble(13, mapMarkerY);
		for(int i = 14; i < 50; i++)
			list.setDouble(i, 0);
		return list;
	}

	public void set(MainData other) {
		setSaveRoom(other.getSaveRoom());
		setDifficulty(other.getDifficulty());
		setGameTime(other.getGameTime());
		setItemsTaken(other.getItemsTaken());
		setETanks(other.getETanks());
		setMTanks(other.getMTanks());
		setSTanks(other.getSTanks());
		setPTanks(other.getPTanks());
		setMetroidsLeft(other.getMetroidsLeft());
		setMetroidsArea(other.getMetroidsArea());
		setLavaState(other.getLavaState());
		setMapMarker(other.isMapMarker());
		setMapMarkerX(other.getMapMarkerX());
		setMapMarkerY(other.getMapMarkerY());
	}

	public int getSaveRoom() {
		return saveRoom;
	}

	public void setSaveRoom(int saveRoom) {
		this.saveRoom = saveRoom;
	}

	public int getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	public double getGameTime() {
		return gameTime;
	}

	public void setGameTime(double gameTime) {
		this.gameTime = gameTime;
	}

	public int getItemsTaken() {
		return itemsTaken;
	}

	public void setItemsTaken(int itemsTaken) {
		this.itemsTaken = itemsTaken;
	}

	public int getETanks() {
		return etanks;
	}

	public void setETanks(int etanks) {
		this.etanks = etanks;
	}

	public int getMTanks() {
		return mtanks;
	}

	public void setMTanks(int mtanks) {
		this.mtanks = mtanks;
	}

	public int getSTanks() {
		return stanks;
	}

	public void setSTanks(int stanks) {
		this.stanks = stanks;
	}

	public int getPTanks() {
		return ptanks;
	}

	public void setPTanks(int ptanks) {
		this.ptanks = ptanks;
	}

	public int getMetroidsLeft() {
		return metroidsLeft;
	}

	public void setMetroidsLeft(int metroidsLeft) {
		this.metroidsLeft = metroidsLeft;
	}

	public int getMetroidsArea() {
		return metroidsArea;
	}

	public void setMetroidsArea(int metroidsArea) {
		this.metroidsArea = metroidsArea;
	}

	public int getLavaState() {
		return lavaState;
	}

	public void setLavaState(int lavaState) {
		this.lavaState = lavaState;
	}

	public boolean isMapMarker() {
		return mapMarker;
	}

	public void setMapMarker(boolean mapMarker) {
		this.mapMarker = mapMarker;
	}

	public double getMapMarkerX() {
		return mapMarkerX;
	}

	public void setMapMarkerX(double mapMarkerX) {
		this.mapMarkerX = mapMarkerX;
	}

	public double getMapMarkerY() {
		return mapMarkerY;
	}

	public void setMapMarkerY(double mapMarkerY) {
		this.mapMarkerY = mapMarkerY;
	}

	@Override
	public String toString() {
		return "MainData[saveRoom=" + saveRoom
			+ ";difficulty=" + difficulty
			+ ";gameTime=" + gameTime
			+ ";itemsTaken=" + itemsTaken
			+ ";etanks=" + etanks
			+ ";mtanks=" + mtanks
			+ ";stanks=" + stanks
			+ ";ptanks=" + ptanks
			+ ";metroidsLeft=" + metroidsLeft
			+ ";metroidsArea=" + metroidsArea
			+ ";lavaState=" + lavaState
			+ ";mapMarker=" + mapMarker
			+ ";mapMarkerX=" + mapMarkerX
			+ ";mapMarkerY=" + mapMarkerY + "]";
	}
}
